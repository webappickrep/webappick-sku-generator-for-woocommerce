<?php
/**
 * Created by PhpStorm.
 * User: md.ohidulislam
 * Date: 7/30/17
 * Time: 5:18 PM
 */

class Webappick_Sku_Generator_For_Woocommerce_Engine{

    /**
     * Save SKU settings
     * @param array $post
     */

    public function save_settings($post){

        # Settings Key
        $options=array(
            "wa_skg_pid",
            "wa_skg_pName_a",
            "wa_skg_pName_b",
            "wa_skg_attr",
            "wa_skg_attr_lt",
            "wa_skg_separator",
            "wa_skg_prefix",
            "wa_skg_suffix",
            "wa_skg_only_empty",
            "wa_skg_auto_sku"
        );

        foreach($options as $key=>$value){
            if(array_key_exists($value,$post)){
                update_option($value,sanitize_text_field($post[$value]));
            }else{
                update_option($value,"");
            }
        }

    }

    /**
     * Get sku settings
     *
     * @return array
     */
    public function get_settings(){

       return  array(
            "wa_skg_pid" => get_option("wa_skg_pid"),
            "wa_skg_pName_a" => get_option("wa_skg_pName_a"),
            "wa_skg_pName_b" => get_option("wa_skg_pName_b"),
            "wa_skg_attr" => get_option("wa_skg_attr"),
            "wa_skg_attr_lt" => get_option("wa_skg_attr_lt"),
            "wa_skg_separator" => get_option("wa_skg_separator"),
            "wa_skg_prefix" => get_option("wa_skg_prefix"),
            "wa_skg_suffix" => get_option("wa_skg_suffix"),
            "wa_skg_only_empty" => get_option("wa_skg_only_empty"),
            "wa_skg_auto_sku" => get_option("wa_skg_auto_sku"),
        );



    }

    public function generate_sku($post_id){

        $sku = "";
        $product = wc_get_product($post_id);

        $id = $product->get_id();

        $settings = $this->get_settings();

        if($settings['wa_skg_pid'] && $settings['wa_skg_pid'] =='y'){

            $sku.=$id;
            $imp[] =$id;
//            echo $sku;
        }


        if($settings['wa_skg_pName_a'] =='y'){

//            $sku.=$id;
            if($settings['wa_skg_pName_b'] =='fl'){

                $name = $product->get_name();
                if($name){
                    $FLS = explode(" ",$name);
                    $str="";
                    foreach($FLS as $FL){
                        $str.=$FL[0];
                    }
                    $sku.=$str;
//                    echo $sku;
//                    echo "<br>";
                    $imp[] =$str;

                }


            }

           elseif($settings['wa_skg_pName_b'] =='slug'){

                $name = $product->get_name();
                if($name){
                    $str = str_replace(" ","-",$name);
                    $sku.=$str;
                    $imp[] =$str;

                }
//               $imp[] =$name;

            }
//            $sku.=$settings['wa_skg_separator'];

//            $imp[] =$sku;
        }


        if($settings['wa_skg_attr']=='y'){
            if($settings['wa_skg_attr_lt']=='fn') {

                $attributes = $product->get_attributes();
                foreach ($attributes as $key=>$attribute) {
                    $color = $product->get_attribute($key);
                    $sku .= $color;
//                    echo $sku;
                    $imp[] =$color;

                }


//
            }

            elseif($settings['wa_skg_attr_lt']=='f2l'){
                $attributes = $product->get_attributes();
                foreach ($attributes as $key=>$attribute) {
                    $color = $product->get_attribute($key);
                    $ftw = substr($color,0,2);
                    $sku.=$ftw;
//                    echo $sku;
                    $imp[] =$ftw;
                }
//

            }
            else{
                $attributes = $product->get_attributes();
                foreach ($attributes as $key=>$attribute) {
                    $color = $product->get_attribute($key);
                    $ftw = substr($color,0,3);
                    $sku.=$ftw;
                    $imp[] =$ftw;
//                    echo $sku;
                }
//



            }
//            $sku.=$settings['wa_skg_separator'];
//            echo $sku;

//            $imp[] =$sku;
//            echo "<pre>";
//            print_r($imp);



        }



       if( $settings['wa_skg_separator']){

           $sku = "";
           $num = count($imp);
           foreach($imp as $key=>$im){


               if($key== $num-1){
                   $sku .= $im.'';
               }
               else{
                   $sku .= $im.$settings['wa_skg_separator'];
               }

           }
//
//             echo $sku;
//


       }
        if($settings['wa_skg_prefix']){

            $sku = $settings['wa_skg_prefix'].$settings['wa_skg_separator'].$sku;
//            echo $sku;
        }
        if($settings['wa_skg_suffix']){

            $sku = $sku.$settings['wa_skg_separator'].$settings['wa_skg_suffix'];

        }
        return strtoupper($sku);
//
//
//
    }
}

function WA_SKU_ENGINE(){
    return new Webappick_Sku_Generator_For_Woocommerce_Engine();
}