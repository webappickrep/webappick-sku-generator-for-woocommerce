<?php

/**
 * Fired during plugin activation
 *
 * @link       https://webappick.com
 * @since      1.0.0
 *
 * @package    Webappick_Sku_Generator_For_Woocommerce
 * @subpackage Webappick_Sku_Generator_For_Woocommerce/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Webappick_Sku_Generator_For_Woocommerce
 * @subpackage Webappick_Sku_Generator_For_Woocommerce/includes
 * @author     Ohidul Islam <wahid0003@gmail.com>
 */
class Webappick_Sku_Generator_For_Woocommerce_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
