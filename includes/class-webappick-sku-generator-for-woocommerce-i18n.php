<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://webappick.com
 * @since      1.0.0
 *
 * @package    Webappick_Sku_Generator_For_Woocommerce
 * @subpackage Webappick_Sku_Generator_For_Woocommerce/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Webappick_Sku_Generator_For_Woocommerce
 * @subpackage Webappick_Sku_Generator_For_Woocommerce/includes
 * @author     Ohidul Islam <wahid0003@gmail.com>
 */
class Webappick_Sku_Generator_For_Woocommerce_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'webappick-sku-generator-for-woocommerce',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
