<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://webappick.com
 * @since      1.0.0
 *
 * @package    Webappick_Sku_Generator_For_Woocommerce
 * @subpackage Webappick_Sku_Generator_For_Woocommerce/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Webappick_Sku_Generator_For_Woocommerce
 * @subpackage Webappick_Sku_Generator_For_Woocommerce/includes
 * @author     Ohidul Islam <wahid0003@gmail.com>
 */
class Webappick_Sku_Generator_For_Woocommerce_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
