(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 */

	 // $(function() {
		//  alert("FECK");
		//  $(document).on('click', '.wa_skg_pid', function () {
		// 	alert("Hi");
		//  });
     //
		//
		//
	 // });

	 /** When the window is loaded:
	 */
	  $( window ).load(function() {

		  var pid="";
		  var pName="";
		  var attr_a="";
		  var attr_b="";
		  var attr_c="";

		  var category="Shoe > Men";
		  var outputA="";
		  var outputB="";
		  var outputC="";
		  var outputD="";


		  var get_wa_skg_pid=$("input[name='wa_skg_pid']").val();
          console.log(get_wa_skg_pid);

		  if(get_wa_skg_pid=="y"){
			  pid="2338";
		  }
		  $(document).on("click", "input[name='wa_skg_pid']", function () {
			  get_wa_skg_pid=$(this).val();
		  });

		  var get_wa_skg_pName_a=$("input[name='wa_skg_pName_a']:checked").val();

		  var get_wa_skg_pName_b=$("input[name='wa_skg_pName_b']:checked").val();

		  var get_wa_skg_attr=$("input[name='wa_skg_attr']:checked").val();

		  var get_wa_skg_attr_lt=$('input[name="wa_skg_attr_lt"]:checked').val();

		  var get_wa_skg_separator=$("input[name='wa_skg_separator']").val();



		  $(document).on("keyup", "input[name='wa_skg_separator']", function () {
			  get_wa_skg_separator=$("input[name='wa_skg_separator']").val();
			  make_wa_sku();
		  });

		  var get_wa_skg_suffix=$("input[name='wa_skg_suffix']").val();
		  if(get_wa_skg_suffix){
			  get_wa_skg_suffix=get_wa_skg_separator+get_wa_skg_suffix;
		  }else {
			  get_wa_skg_suffix="";
		  }


		  $(document).on("keyup", "input[name='wa_skg_suffix']", function () {
			  if($(this).val()){
				  get_wa_skg_suffix=get_wa_skg_separator+$(this).val();
				  make_wa_sku();
			  }else {
				  get_wa_skg_suffix="";
				  make_wa_sku();
			  }
		  });


		  var get_wa_skg_prefix=$("input[name='wa_skg_prefix']").val();
		  if(get_wa_skg_prefix){
			  get_wa_skg_prefix=get_wa_skg_prefix+get_wa_skg_separator;
		  }else {
			  get_wa_skg_prefix="";
		  }

		  $(document).on("keyup", "input[name='wa_skg_prefix']", function () {
			  if($(this).val()){
				  get_wa_skg_prefix=$(this).val()+get_wa_skg_separator;
				  make_wa_sku();
			  }else {
				  get_wa_skg_prefix="";
				  make_wa_sku();
			  }

		  });



		  $(document).on("click", ".wa_sku_settings", function () {

                get_wa_skg_pName_a=$("input[name='wa_skg_pName_a']:checked").val();
                get_wa_skg_pName_b=$("input[name='wa_skg_pName_b']:checked").val();
                get_wa_skg_attr=$("input[name='wa_skg_attr']:checked").val();
                get_wa_skg_attr_lt=$('input[name="wa_skg_attr_lt"]:checked').val();
                get_wa_skg_separator=$("input[name='wa_skg_separator']").val();
                get_wa_skg_prefix=$("input[name='wa_skg_prefix']").val();
                get_wa_skg_suffix=$("input[name='wa_skg_suffix']").val();

			  if(get_wa_skg_prefix){
				  get_wa_skg_prefix=get_wa_skg_prefix+get_wa_skg_separator;
			  }else {
				  get_wa_skg_prefix="";
			  }

			  if(get_wa_skg_suffix){
				  get_wa_skg_suffix=get_wa_skg_separator+get_wa_skg_suffix;
			  }else {
				  get_wa_skg_suffix="";
			  }

			    make_wa_sku();
		  });

		  make_wa_sku();

		  function make_wa_sku() {
			  if(get_wa_skg_pid=='y'){
				  pid="2833";
			  }else{
				  pid="";
			  }

			  if(get_wa_skg_pName_a=='y'){
				  pName="Your Product Title";
				  pName=pName+get_wa_skg_separator;
			  }else{
				  pName="";
			  }

			  if(get_wa_skg_pName_a=='y' && get_wa_skg_pName_b=='fl'){
				  var matches = pName.match(/\b(\w)/g);
				  pName = matches.join('');
				  pName=pName+get_wa_skg_separator;
			  }else if(get_wa_skg_pName_a=='y' && get_wa_skg_pName_b=='slug'){
				  pName=pName.replace(/ /g, "-").toUpperCase();
				  pName=pName+get_wa_skg_separator;
			  }

			  if(get_wa_skg_attr=='y'){
				  attr_a="Green".toUpperCase();
				  attr_b="Blue".toUpperCase();
				  attr_c="Red".toUpperCase();

				  attr_a=attr_a+get_wa_skg_separator;
				  attr_b=attr_b+get_wa_skg_separator;
				  attr_c=attr_c+get_wa_skg_separator;
			  }else{
				  attr_a="";
				  attr_b="";
				  attr_c="";
			  }


			  if(get_wa_skg_attr=='y' && get_wa_skg_attr_lt=='fn'){

				  attr_a=attr_a.toUpperCase();
				  attr_b=attr_b.toUpperCase();
				  attr_c=attr_c.toUpperCase();

			  }else if(get_wa_skg_attr=='y' && get_wa_skg_attr_lt=='f2l'){

				  var xa= attr_a.split(' ');
				  attr_a = xa[0].substring(0,2);
				  attr_a=attr_a.toUpperCase();

				  var xb= attr_b.split(' ');
				  attr_b = xb[0].substring(0,2);
				  attr_b=attr_b.toUpperCase();

				  var xc= attr_c.split(' ');
				  attr_c = xc[0].substring(0,2);
				  attr_c=attr_c.toUpperCase();

			  }else if(get_wa_skg_attr=='y' && get_wa_skg_attr_lt=='f3l'){

				  var x2a= attr_a.split(' ');
				  attr_a = x2a[0].substring(0,3);
				  attr_a=attr_a.toUpperCase();

				  var x2b= attr_b.split(' ');
				  attr_b = x2b[0].substring(0,3);
				  attr_b=attr_b.toUpperCase();

				  var x2c= attr_c.split(' ');
				  attr_c = x2c[0].substring(0,3);
				  attr_c=attr_c.toUpperCase();

			  }

              console.log(get_wa_skg_prefix);
              console.log(pName);
			  outputA=get_wa_skg_prefix+pName+attr_a+pid+get_wa_skg_suffix;
			  outputB=get_wa_skg_prefix+pName+attr_a+pid+get_wa_skg_suffix;
			  outputC=get_wa_skg_prefix+pName+attr_b+pid+get_wa_skg_suffix;
			  outputD=get_wa_skg_prefix+pName+attr_c+pid+get_wa_skg_suffix;

			  wa_skg_preview();
		  }

		  function wa_skg_preview() {
			  $("#wa_skg_simple_preview").text(outputA);
			  $("#wa_skg_variation_preview_a").text(outputB);
			  $("#wa_skg_variation_preview_b").text(outputC);
			  $("#wa_skg_variation_preview_c").text(outputD);
			  outputA="";
			  outputB="";
			  outputC="";
			  outputD="";
		  }

		  //----------------------------------------------//
          // SKU generate
          var width ;
          $('.wpf_sku_generate').on("click",function (e) {
              width = 10;
              $("#skuprogresstable").hide();
              $(this).text('Generating...');
              $('div i').addClass('dashicons-sos wpf_sos').removeClass('dashicons-yes');
              showSKUProgress();
              $(".wpf_sku_generate").prop('disabled', true);
			  $("#skuprogresstable").show();
			  generate_sku();

          });


          function showSKUProgress(color = "#3dc264"){
              // Progress br init
              var bar = document.querySelector('.sku-progress-bar-fill');
              bar.style.width = width + '%';
              bar.style.background =color;
              var nWidth=Math.round(width);
              $(".sku-progress-container3").text(nWidth + '%');
          }


          function generate_sku() {
              $(".sku-progress-container2").text("Calculating total products.");

              $.ajax({
                  url : wpf_ajax_obj.wpf_ajax_url,
                  type : 'post',
                  data : {
                      _ajax_nonce: wpf_ajax_obj.nonce,
                      action: "get_product_information",
                  },
                  success : function(response) {
                      if(response.success) {
                          $(".sku-progress-container2").text("Delivering SKU Configuration.");
                          var products=parseInt(response.data.product);
                          processSKU(products);

                          $(".sku-progress-container2").text("Processing Products...");
                      }else{
                          $(".sku-progress-container2").text(response.data.message);
                          showSKUProgress('red');
                      }
                  }
              });
          }

          var limit =100;
          function processSKU(n,offset,batch) {
              if (typeof(offset)==='undefined') offset = 0;
              if (typeof(batch)==='undefined') batch = 0;

              var batches =Math.ceil(n/limit);

              var progressBatch=90/batches;

              var currentProducts=limit*batch;


              var nWidth=Math.round(width);
              $(".sku-progress-container2").text("Processing products..."+nWidth+"%");

              if(batch<batches){

                  $.ajax({
                      url : wpf_ajax_obj.wpf_ajax_url,
                      type : 'post',
                      data : {
                          _ajax_nonce: wpf_ajax_obj.nonce,
                          action: "webappick_generate_update_SKU",
                          limit:limit,
                          offset:offset,
                      },
                      success : function(response) {

                          if(response.success) {
                              if(response.data.products=="yes"){
                                  offset=offset+limit;
                                  batch++;
                                  setTimeout(function(){
                                      processSKU(n,offset,batch);
                                  }, 2000);

                                  width=width+progressBatch;
                                  showSKUProgress();
                              }else if(n>offset){
                                  offset=offset+limit;
                                  batch++;
                                  processSKU(n,offset,batch);

                                  width=width+progressBatch;
                                  showSKUProgress();
                              }else{
                                  $(".sku-progress-container2").text("Products SKU Successfully Generated.");
                                  $('div i').removeClass('dashicons-sos wpf_sos').addClass('dashicons-yes');
                                  $(".wpf_sku_generate").prop('disabled', false);

                              }
                          }
                      },
                      error:function (response) {
                          if(response.status!=="200"){

                              offset=(offset-limit)+10
                              batch++;
                              processSKU(n,offset,batch);

                              width=width+progressBatch;
                              showSKUProgress();
                          }
                      }
                  });
              }else{
                  $(".sku-progress-container2").text("Products SKU Generated Successfully.");
                  $('div i').removeClass('dashicons-sos wpf_sos').addClass('dashicons-yes');
                  $(".wpf_sku_generate").prop('disabled', false);
              }
          }


      });
	 /**
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

})( jQuery );
