<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://webappick.com
 * @since      1.0.0
 *
 * @package    Webappick_Sku_Generator_For_Woocommerce
 * @subpackage Webappick_Sku_Generator_For_Woocommerce/admin/partials
 */

?>

<div class="wrap">


    <div id="icon-options-general" class="icon32"></div>
    <h1><?php esc_attr_e( 'Product SKU Generator', 'webappick-sku-generator-for-woocommerce' ); ?></h1>

    <table class="table widefat fixed" id="skuprogresstable" style="display: none;">
        <thead>
        <tr>
            <th class="sku_successfully_completed_status"><b></b></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <div class="sku-progress-container">
                    <div class="sku-progress-bar" >
                        <span class="sku-progress-bar-fill"></span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="float: left;"><b style='color: darkblue;'><i class='dashicons dashicons-sos wpf_sos'></i></b>&nbsp;&nbsp;&nbsp;</div>
                <div class="sku-progress-container2" style="float: left;font-weight: bold;color: darkblue;">

                </div>
                <div class="sku-progress-container3" style="text-align:right;font-weight: bolder;color: #41f49d;font-family:'Arial Black';font-size: large;">

                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <div id="poststuff">

        <div id="post-body" class="metabox-holder columns-2">

            <!-- main content -->
            <div id="post-body-content">

                <div class="meta-box-sortables ui-sortable">

                    <div class="postbox">

                        <div class="handlediv" title="Click to toggle"><br></div>
                        <!-- Toggle -->

                        <h2 class="hndle"><span><?php esc_attr_e( 'SKU Settings', 'webappick-sku-generator-for-woocommerce' ); ?></span>
                        </h2>

                        <div class="inside">
                            <form action="" method="post">
                                <table class="form-table" id="wa_sku_settings">
                                    <tr valign="top"  class="alternate">
                                        <td scope="row"><label for="tablecell"><?php esc_attr_e('Product Id', 'webappick-sku-generator-for-woocommerce'); ?></label>
                                        </td>
                                        <td scope="row">
                                            <input type="radio" class="wa_sku_settings wa_skg_pid" <?php checked(get_option("wa_skg_pid"),'y',true);  ?> value="y" name="wa_skg_pid" ><?php esc_attr_e('Yes', 'webappick-sku-generator-for-woocommerce'); ?>
                                            <input type="radio" class="wa_sku_settings wa_skg_pid" <?php checked(get_option("wa_skg_pid"),'n',true); ?> value="n" name="wa_skg_pid"  ><?php esc_attr_e('No', 'webappick-sku-generator-for-woocommerce'); ?>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td scope="row"><label for="tablecell"><?php esc_attr_e('Product Title', 'webappick-sku-generator-for-woocommerce'); ?></label>
                                        </td>
                                        <td scope="row">
                                            <input type="radio" value="y" class="wa_sku_settings" <?php checked(get_option("wa_skg_pName_a"),'y',true); ?> name="wa_skg_pName_a"><?php esc_attr_e('Yes', 'webappick-sku-generator-for-woocommerce'); ?>
                                            <input type="radio" value="n" class="wa_sku_settings" <?php checked(get_option("wa_skg_pName_a"),'n',true); ?> name="wa_skg_pName_a" ><?php esc_attr_e('No', 'webappick-sku-generator-for-woocommerce'); ?>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td scope="row"><label for="tablecell"><?php esc_attr_e('', 'webappick-sku-generator-for-woocommerce'); ?></label>
                                        </td>
                                        <td scope="row">
                                            <input type="radio" class="wa_sku_settings" <?php checked(get_option("wa_skg_pName_b"),'fl',true); ?> value="fl" name="wa_skg_pName_b"><?php esc_attr_e('First Letters', 'webappick-sku-generator-for-woocommerce'); ?>
                                            <input type="radio" class="wa_sku_settings" <?php checked(get_option("wa_skg_pName_b"),'slug',true); ?> value="slug" name="wa_skg_pName_b"><?php esc_attr_e('Slug', 'webappick-sku-generator-for-woocommerce'); ?>
                                        </td>
                                    </tr>
                                    <tr valign="top"  class="alternate">
                                        <td scope="row"><label for="tablecell"><?php esc_attr_e('Attributes', 'webappick-sku-generator-for-woocommerce'); ?></label>
                                        </td>
                                        <td scope="row">
                                            <input type="radio" class="wa_sku_settings" value="y" <?php checked(get_option("wa_skg_attr"),'y',true); ?> name="wa_skg_attr"><?php esc_attr_e('Yes', 'webappick-sku-generator-for-woocommerce'); ?>
                                            <input type="radio" class="wa_sku_settings" value="n" <?php checked(get_option("wa_skg_attr"),'n',true); ?> name="wa_skg_attr"><?php esc_attr_e('No', 'webappick-sku-generator-for-woocommerce'); ?>
                                        </td>
                                    </tr>
                                    <tr valign="top"  class="alternate">
                                        <td scope="row"><label for="tablecell"><?php esc_attr_e('', 'webappick-sku-generator-for-woocommerce'); ?></label>
                                        </td>
                                        <td scope="row">
                                            <input type="radio" class="wa_sku_settings" value="fn" <?php checked(get_option("wa_skg_attr_lt"),'fn',true); ?>  name="wa_skg_attr_lt"><?php esc_attr_e('Full Name', 'webappick-sku-generator-for-woocommerce'); ?>
                                            <input type="radio" class="wa_sku_settings" value="f2l" <?php checked(get_option("wa_skg_attr_lt"),'f2l',true); ?>  name="wa_skg_attr_lt"><?php esc_attr_e('First 2 Letter', 'webappick-sku-generator-for-woocommerce'); ?>
                                            <input type="radio" class="wa_sku_settings" value="f3l" <?php checked(get_option("wa_skg_attr_lt"),'f3l',true); ?>  name="wa_skg_attr_lt"><?php esc_attr_e('First 3 Letter', 'webappick-sku-generator-for-woocommerce'); ?>
                                        </td>
                                    </tr>
                                    <tr valign="top" class="alternate">
                                        <td scope="row"><label for="tablecell"><?php esc_attr_e('Separator', 'webappick-sku-generator-for-woocommerce'); ?></label>
                                        </td>
                                        <td scope="row">
                                            <input type="text" class="wa_sku_settings wa_skg_separator" value="<?php echo (get_option("wa_skg_separator"))? esc_attr(get_option("wa_skg_separator")):""; ?>" name="wa_skg_separator">
                                        </td>
                                    </tr>
                                    <tr valign="top" class="alternate">
                                        <td scope="row"><label for="tablecell"><?php esc_attr_e('Prefix', 'webappick-sku-generator-for-woocommerce'); ?></label>
                                        </td>
                                        <td scope="row">
                                            <input type="text" class="wa_sku_settings" value="<?php echo (get_option("wa_skg_prefix"))? esc_attr(get_option("wa_skg_prefix")):""; ?>" name="wa_skg_prefix">
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td scope="row"><label for="tablecell"><?php esc_attr_e('Suffix', 'webappick-sku-generator-for-woocommerce'); ?></label>
                                        </td>
                                        <td scope="row">
                                            <input type="text" class="wa_sku_settings" name="wa_skg_suffix" value="<?php echo (get_option("wa_skg_suffix"))? esc_attr(get_option("wa_skg_suffix")):""; ?>">
                                        </td>
                                    </tr>
                                    <tr valign="top" class="alternate">
                                        <td scope="row"><label for="tablecell"><?php esc_attr_e('Only Generate For Products with Empty SKU', 'webappick-sku-generator-for-woocommerce'); ?></label>
                                        </td>
                                        <td scope="row">
                                            <input type="checkbox" value="1" name="wa_skg_only_empty" <?php checked(get_option("wa_skg_only_empty"),1,true); ?> > <?php esc_attr_e('Enable', 'webappick-sku-generator-for-woocommerce'); ?>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td scope="row"><label for="tablecell"><?php esc_attr_e('Auto Generate SKU for New Product', 'webappick-sku-generator-for-woocommerce'); ?></label>
                                        </td>
                                        <td scope="row">
                                            <input type="checkbox" <?php checked(get_option("wa_skg_auto_sku"),'on',true); ?> name="wa_skg_auto_sku"><?php esc_attr_e('Enable', 'webappick-sku-generator-for-woocommerce'); ?>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td scope="row"><label for="tablecell"></label>
                                        </td>
                                        <td scope="row">
                                            <input type="submit" class="button button-primary" name="wa_sku_setting_save" value="<?php esc_attr_e('Save Settings', 'webappick-sku-generator-for-woocommerce'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="submit" class="button button-primary" name="wa_sku_preview" value="<?php esc_attr_e('Generate Original Preview', 'webappick-sku-generator-for-woocommerce'); ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="button" class="button button-primary wpf_sku_generate" id="wpf_sku_generate" name="wa_sku_generate" value="<?php esc_attr_e('Generate SKU', 'webappick-sku-generator-for-woocommerce'); ?>">
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <!-- .inside -->

                    </div>
                    <!-- .postbox -->

                </div>
                <!-- .meta-box-sortables .ui-sortable -->

            </div>
            <!-- post-body-content -->

            <!-- sidebar -->
            <div id="postbox-container-1" class="postbox-container">

                <div class="meta-box-sortables">

                    <div class="postbox">

                        <div class="handlediv" title="Click to toggle"><br></div>
                        <!-- Toggle -->

                        <h2 class="hndle"><span><?php esc_attr_e(
                                    'Sample SKU Preview', 'webappick-sku-generator-for-woocommerce'
                                ); ?></span></h2>

                        <div class="inside">
                            <table class="form-table">
                                <tr>
                                    <td >Product Id: <b>2338</b><br><br>Product Title: <b>Your Product Title</b> <br><br>Attributes: <b>Green,Blue,Red</b></td>
                                </tr>
                                <tr>
                                    <th class="row-title" style="color: #00b0ff"><?php esc_attr_e( 'Simple Product', 'webappick-sku-generator-for-woocommerce' ); ?></th>
                                </tr>
                                <tr valign="top">
                                    <td scope="row"><b><span id="wa_skg_simple_preview"></span></b>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="row-title" style="color: #00b0ff"><?php esc_attr_e( 'Variable Product', 'webappick-sku-generator-for-woocommerce' ); ?></th>
                                </tr>
                                <tr valign="top">
                                    <td scope="row"><b><span id="wa_skg_variation_preview_a"></span></td>
                                </tr>
                                <tr valign="top">
                                    <td scope="row"><b><span id="wa_skg_variation_preview_b"></span></td>
                                </tr>
                                <tr valign="top">
                                    <td scope="row"><b><span id="wa_skg_variation_preview_c"></span></td>
                                </tr>
                            </table>
                        </div>
                        <!-- .inside -->

                    </div>
                    <!-- .postbox -->

                </div>
                <!-- .meta-box-sortables -->

            </div>
            <!-- #postbox-container-1 .postbox-container -->

        </div>
        <!-- #post-body .metabox-holder .columns-2 -->

        <br class="clear">
    </div>
    <!-- #poststuff -->

    <?php
    # Get Preview Table if Preview Button submit
    if(isset($_POST['wa_sku_preview'])){
	    webappick_get_preview_text();
    }
//    echo webappick_getAllProducts();
    ?>

</div> <!-- .wrap -->

<!-- This file should primarily consist of HTML with a little bit of PHP. -->