<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://webappick.com
 * @since      1.0.0
 *
 * @package    Webappick_Sku_Generator_For_Woocommerce
 * @subpackage Webappick_Sku_Generator_For_Woocommerce/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Webappick_Sku_Generator_For_Woocommerce
 * @subpackage Webappick_Sku_Generator_For_Woocommerce/admin
 * @author     Ohidul Islam <wahid0003@gmail.com>
 */
class Webappick_Sku_Generator_For_Woocommerce_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Webappick_Sku_Generator_For_Woocommerce_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Webappick_Sku_Generator_For_Woocommerce_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/webappick-sku-generator-for-woocommerce-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Webappick_Sku_Generator_For_Woocommerce_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Webappick_Sku_Generator_For_Woocommerce_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/webappick-sku-generator-for-woocommerce-admin.js', array( 'jquery' ), $this->version, false );

        $wpf_sku_nonce = wp_create_nonce('wpf_sku_nonce');
        wp_localize_script($this->plugin_name, 'wpf_ajax_obj', array(
            'wpf_ajax_url' => admin_url('admin-ajax.php'),
            'nonce' => $wpf_sku_nonce,
        ));
	}

	/**
	 * Register the Plugin's Admin Pages for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function load_admin_pages()
	{
		/**
		 * This function is provided for making admin pages into admin area.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Webappick_Sku_Generator_For_Woocommerce_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Webappick_Sku_Generator_For_Woocommerce_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if (function_exists('add_options_page')) {

			add_menu_page(__('SKU Generator', 'webappick-sku-generator-for-woocommerce'), __('SKU Generator', 'webappick-sku-generator-for-woocommerce'), 'manage_options', __FILE__, 'webappick_sku_generate', 'dashicons-tag');
			add_submenu_page(__FILE__, __('Generate', 'webappick-sku-generator-for-woocommerce'), __('Generate', 'webappick-sku-generator-for-woocommerce'), 'manage_options', __FILE__, 'webappick_sku_generate');
		}
	}

}
