<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://webappick.com
 * @since             1.0.0
 * @package           Webappick_Sku_Generator_For_Woocommerce
 *
 * @wordpress-plugin
 * Plugin Name:       WebAppick SKU Generator for WooCommerce
 * Plugin URI:        https://webappick.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Ohidul Islam
 * Author URI:        https://webappick.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       webappick-sku-generator-for-woocommerce
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-webappick-sku-generator-for-woocommerce-activator.php
 */
function activate_webappick_sku_generator_for_woocommerce() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-webappick-sku-generator-for-woocommerce-activator.php';
	Webappick_Sku_Generator_For_Woocommerce_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-webappick-sku-generator-for-woocommerce-deactivator.php
 */
function deactivate_webappick_sku_generator_for_woocommerce() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-webappick-sku-generator-for-woocommerce-deactivator.php';
	Webappick_Sku_Generator_For_Woocommerce_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_webappick_sku_generator_for_woocommerce' );
register_deactivation_hook( __FILE__, 'deactivate_webappick_sku_generator_for_woocommerce' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-webappick-sku-generator-for-woocommerce.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_webappick_sku_generator_for_woocommerce() {

	$plugin = new Webappick_Sku_Generator_For_Woocommerce();
	$plugin->run();

}
run_webappick_sku_generator_for_woocommerce();

function webappick_sku_generate(){

	# Save SKU Settings
    if(isset($_POST['wa_sku_setting_save']) || isset($_POST['wa_sku_preview'])) {
        WA_SKU_ENGINE()->save_settings($_POST);
    }
	if(isset($_POST['wa_sku_generate'])){
		webappick_generate_update_SKU();

	}
	require plugin_dir_path(__FILE__) . 'admin/partials/webappick-sku-generator-for-woocommerce-admin-display.php';

}



//modified by ajax call

add_action('wp_ajax_get_product_information', 'webappick_sku_get_product_information');

function webappick_sku_get_product_information(){
    $products = wc_get_products( array(
        'limit' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
    ) );

    $totalProducts=count($products);

    $data=array('product'=>$totalProducts);

    if($totalProducts>0){
        $data['success']=true;
        wp_send_json_success($data);
    }else{
        $data['success']=false;
        $data['message']="Product not found";
        wp_send_json_error($data);
    }

    wp_die();
}
add_action('wp_ajax_webappick_generate_update_SKU', 'webappick_generate_update_SKU');

function webappick_generate_update_SKU(){

		$products = wc_get_products( array(
			'limit' => $_POST['limit'],
			'orderby' => 'date',
			'order' => 'DESC',
            'offset' => $_POST['offset'],
		) );


		foreach($products as $key=> $product){
			if(!is_object($product)){
				continue;
			}
			$id = $product->get_id();
			$sku = WA_SKU_ENGINE()->generate_sku($id);
			$status = update_post_meta($id,"_sku",$sku);

		}
    if($status){
        $data=array(
            "success"=>true,
            "products"=>"yes",
        );
        wp_send_json_success($data);
    }else{
        $data=array(
            "success"=>true,
            "products"=>"no",
        );
        wp_send_json_success($data);
    }
    wp_die();

}


function webappick_get_preview_text(){
	
	$query =  array(
		'limit' => 5,
		'status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',

	);

	$products = wc_get_products($query);

	if(sizeof($products) > 0 ){

		$preview= "<table class='widefat' border='1' style='width: 84%'>
			<thead>
            	<tr>
	                <th>Product Name</th>
	                <th>Attribute</th>
	                <th>SKU</th>
            	</tr>
            </thead>
            ";
		$preview .= '<tbody>';
		foreach($products as $key=>$product)
		{
			if(!is_object($product)){
				continue;
			}
			
			$attributes = $product->get_attributes();
			$preview .= ' <tr>';
			$preview .= '<td>'.$product->get_title().'</td>';

			$count= 0;
			$preview .= '<td>';
			if($attributes){
				foreach ($attributes as $attr_key=>$attribute) {
					$attr = $product->get_attribute($attr_key);
					if($count==0)
						$preview .= $attr;
					else
						$preview .= ",".$attr;
					$count++;
				}
			}
			$preview .= '</td>';

			$preview .= '<td>'.WA_SKU_ENGINE()->generate_sku($product->get_id()).'</td>';

			$preview .= '</tr>';
		}
		$preview .= '</tbody>';
		$preview .= "</table> ";
	}

	echo $preview;
}

function webappick_countAllProducts(){

    $products = wc_get_products( array(
        'limit' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
        'return' => 'ids',
    ) );
    return count($products);
    
}

add_action( 'updated_post_meta', 'webappick_sku_for_new_product_meta', 10, 4 );
function webappick_sku_for_new_product_meta( $meta_id, $post_id, $meta_key, $meta_value ) {
	if ( $meta_key == '_sku' ) {
		if ( get_post_type( $post_id ) == 'product' ) {
            $sku = WA_SKU_ENGINE()->generate_sku($post_id);
			update_post_meta($post_id,"_sku",$sku);
		}
	}
}

add_action('save_post_product', 'webappick_sku_for_new_products', 10, 3);
function webappick_sku_for_new_products( $post_id, $post, $update ) {
	$sku = WA_SKU_ENGINE()->generate_sku($post_id);
	update_post_meta($post_id,"_sku",$sku);
}